import emfwebcrawler as emfwc
import numpy as np
import sys

print('write url (default: \'https://www.ukf.sk\')')
url_to_analyze = input()
url_to_analyze = url_to_analyze or 'https://www.ukf.sk'

print('write depth (default: 2)')
depth_to_analyze = input()
depth_to_analyze = depth_to_analyze or 2
depth_to_analyze = int(depth_to_analyze)

# if math.isnan(depth_to_analyze) or depth_to_analyze < 1:
#     sys.exit("depth is not a positive number")

result, matrix = emfwc.analyze(url_to_analyze, depth_to_analyze)
# result, matrix = analyzer.analyze("https://www.ki.fpv.ukf.sk", 4)
# result, matrix = analyzer.analyze("https://www.sme.sk", 2)
# result, matrix = analyzer.analyze("https://www.ukf.sk", 4)

# zapne uplny vypis matic a poli
np.set_printoptions(threshold=sys.maxsize)

print('result:')
print(result.to_string())
print('matrix:')
print(matrix)

web_matrix = emfwc.prepare_matrix_for_pagerank(matrix)
print('web_matrix')
print(web_matrix)
print('page rank')
pr = emfwc.pagerank(web_matrix)

print('PR:', pr)
